#!/bin/bash

export RECAST_USER=<name>
export RECAST_PASS=<password>
export RECAST_TOKEN=<token>

eval "$(recast auth setup -a $RECAST_USER -a $RECAST_PASS -a $RECAST_TOKEN -a default)"
eval "$(recast auth write --basedir authdir)"



$(recast catalogue add $PWD)
recast catalogue ls
recast catalogue describe examples/helloworld_test
recast catalogue check examples/helloworld_test

# clean up
rm -rf recast-myrun

# run the workflow
recast run examples/helloworld_test --tag myrun --backend docker
